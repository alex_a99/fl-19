//1
 const even = (arr) => {
    let two = 2
    let res = arr.filter(n => n % two ===0)
  return Math.max(...res)
  
}


console.log(even(['1','2','4','7']))

//2

const swapVariable = (a,b) => {
return (
    [a,b] = [b,a]
) 
}


//3
const getValue = (value) => {
    return value ?? '-'
}



//4

const getObjFromArray = (arr) => {
    return Object.fromEntries(arr)
}

console.log(getObjFromArray([['name','dan'],
                            ['age','21'],
                            ['city','lviv']]))


//5

const addUniqueId = (obj) => {
    return Object.assign(obj,{id:Symbol()})
}


console.log(addUniqueId({name:'nick'}))
//6

const getRegroupedObject = ({details, name}) => { 
    return { 
        university: details.uni, 
        user: { 
            age: details.age, 
            firstName: name, 
            id: details.id 
        } 
    } 
}
console.log(getRegroupedObject({
    name : 'willow',
    details: {
        id:1,
        age:47,
        uni:'LNU'

    }
}))
///////

//7
 
const getArrayWithUniqueElement = (arr) => {
    let newArr = new Set([...arr])
    return newArr
}




//8

const hideNumber = (num) => {
    let ten = 10
    let six = 6
    let newStr = num.split('').splice(six,num.length).join('')
return newStr.padStart(ten,'*')
}

console.log(hideNumber('0123456789'))


//9 


const add = (a,b) => {
    if(b === undefined){
        throw new Error('b is required')
    } else if (a === undefined){
       throw new Error('a is required')
    }else{
        return a+b
    }   
}





//10 
function* generateIterableSequence(){
    yield 'I'
    yield 'love'
    yield 'EPAM'
}

const generatorObject = generateIterableSequence()

for(let value of generatorObject){
    console.log(value)
}

let three = 3

generateIterableSequence(three)