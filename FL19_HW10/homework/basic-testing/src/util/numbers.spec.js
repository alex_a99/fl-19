/* eslint-disable no-undef */
import {describe, expect, test} from 'vitest';
import {transformToNumber} from './numbers.js'


describe('transformformToNumber positive',() => {

        test(`tranformToNumber 5`, () => {
            expect(transformToNumber(5)).toBe(5)
        })

    test(`tranformToNumber ""`, () => {
            expect(transformToNumber('')).toBe(0)
        })


        test(`tranformToNumber "-5"`, () => {
            expect(transformToNumber('-5')).toBe(-5)
        })

        test(`tranformToNumber null`, () => {
            expect(transformToNumber(null)).toBe(0)
        })
    })


