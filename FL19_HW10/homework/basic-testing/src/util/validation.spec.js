import {describe,test,beforeEach,expect} from 'vitest';
import {validateNumber, validateStringNotEmpty} from './validation';


describe('test validateNumber function',() => {
    let testNum = 0
    beforeEach(() => {
        testNum = Math.floor(Math.random() * 101);
    })
    test('have to return num',() => {
        expect(validateNumber(testNum)).toEqual(testNum)
    })
    test('have to return num',() => {
        expect(validateNumber(testNum)).toEqual(testNum)
    })
    test('have to return num',() => {
        expect(validateNumber(testNum)).toEqual(testNum)
    })
})

describe('test validateNum Function if NaN', () => {
    test('test thTrow in func validate num',() => {
        expect(() => validateNumber('uyreighfeuirh')).toThrow()
    })
})


describe('test func validateStringNotEmpty',() => {
    const emptyStr = ''
    const notEmptyStr = 'I am not empty string'

    test('test func with emptyStr', () => {
        expect(() => validateStringNotEmpty(emptyStr)).toThrow()
    })
    test('test func with notEmptyStr', () => {
        expect(validateStringNotEmpty(notEmptyStr)).toEqual(notEmptyStr)
    })


})