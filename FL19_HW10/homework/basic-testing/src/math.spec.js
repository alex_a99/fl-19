import {describe, expect,it} from 'vitest';
import {add} from './math';

describe('check positive scenarios', () => {
    it('sum of num', () => {
        expect(add([1,6])).equal(7)
        expect(add([1,7])).equal(8)
        expect(add([1,2,3])).toBe(6)
    })
})

