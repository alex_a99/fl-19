import {describe,test,expect} from 'vitest';
import {extractNumbers} from './parser';
import {JSDOM} from 'jsdom'

describe('extract num',() => {

    test('have to return key', () => {
        const jsdom = new JSDOM('')
        const formData = new jsdom.window.FormData()
        formData.append('num2','4')
        formData.append('num1','3')
        expect(extractNumbers(formData)).toStrictEqual(['3','4'])
    })
    test('have to return key2', () => {
        const jsdom = new JSDOM('')
        const formData = new jsdom.window.FormData()
        formData.append('num2','8')
        formData.append('num1','9')
        expect(extractNumbers(formData)).toStrictEqual(['9','8'])
    })
})