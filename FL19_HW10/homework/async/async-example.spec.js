import {describe,test,expect} from 'vitest';
import {generateToken,generateTokenPromise} from './async-example.js';
import jwt from 'jsonwebtoken';

describe('get token for email', () => {


test('generate token',() => {
    const callResult = {
      email: 'test@test.com',
      password: 'password',
      callBack : function callBack (err, token) {
 token 
}
    }

    generateToken('test@test.com', callResult.callBack)
    expect(callResult.email).toBe('test@test.com')
    expect(callResult.password).toBe('password')
},1000)

})
describe('generate token promise', () => {
    let email = 'test@test.com'

            test('generate promise resolve', async() => {
                jwt.sign({ email }, 'password', (error, token) => {
                    expect(generateTokenPromise(email)).resolves.toBe(token)
                })
            },1000)
        test('generate promise reject', async () => {
            jwt.sign({ email }, 'password', () => {
                expect(generateTokenPromise(email)).toThrowError('error')
            })
        },1000)
    }
)