const path = require('path')
const miniCss = require('mini-css-extract-plugin');

const HTMLWebpackPlugin = require('html-webpack-plugin')
module.exports = {
    mode: 'production',
    entry: {
        main: './js/index.js',
    },
    output : {
        filename: 'bundle.js',
        path: path.resolve(__dirname,'dist')
    },
    plugins:
        [
            new HTMLWebpackPlugin({
                template: './index.html'
            }),
            new miniCss({
                filename: 'style.css',
             })
            
        ],
    module:{
        
        rules: [
            {
                test:/\.scss$/,
                use: [
                   miniCss.loader,
                   'css-loader',
                   'sass-loader',
                
                 
                ]}
    ]

    }
        
    
}