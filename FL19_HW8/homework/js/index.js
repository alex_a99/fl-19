//1

class ToysFactory {
    constructor(name, price) {
        this.name = name
        this.price = price
    }

    produce(name, price, material){
        switch (material) {
            case 'teddy': return new TeddyToy(name, price);
            case 'wooden': return new WoodenToy(name, price);
            case 'plastic': return new PlasticToy(name, price);
            default: return new PlasticToy(name, price)
        }
    }

}

class TeddyToy extends ToysFactory{
        constructor(name, price) {
            super(name, price)
            this.material = 'cotton'
            this.name = name
        this.price = price

    }

    getMaterialInfo(){
        return `The toy ${this.name} was made of ${this.material}.`
    }
    getToyInfo(){
        return `The toy name is ${this.name}. It costs ${this.price} dollars`
    }
}

let instance
class WoodenToy extends ToysFactory {
    constructor(name, price) {
        super(name, price)
        this.material = 'wood';
        this.name = name
        this.price = price
        if(typeof WoodenToy.instance === 'object'){
            return WoodenToy.instance
        } else {
            WoodenToy.instance = this
        }
    }
    getMaterialInfo(){
        return `The toy ${this.name} was made of ${this.material}.`
    }
    getToyInfo(){
        return `The toy name is ${this.name}. It costs ${this.price} dollars`
    }
}
class PlasticToy extends ToysFactory{
    constructor(name,price) {
        super(name, price);
        this.material = 'plastic'
        this.name = name
        this.price = price
    }
    getMaterialInfo(){
        return `The toy ${this.name} was made of ${this.material}.`
    }
    getToyInfo(){
        return `The toy name is ${this.name}. It costs ${this.price} dollars`
    }
}
const factory = new ToysFactory()
const teddyToy = factory.produce('Bear',200,'teddy')
console.log('1.1')
console.log(teddyToy.getToyInfo())
console.log(teddyToy.getMaterialInfo())
const plasticCar = factory.produce('car',300)
console.log(plasticCar.getToyInfo())
console.log(plasticCar.getMaterialInfo())
console.log('1.2')
const teddyBear = factory.produce('Bear',200,'teddy')
console.log(teddyBear.getToyInfo())
console.log(teddyBear.getMaterialInfo())
const plasticBear = factory.produce('Bear',150,'plastic')
console.log(plasticBear.getToyInfo())
console.log(plasticBear.getMaterialInfo())
console.log('1.3')
const woodenHorse = factory.produce('Horse',400,'wooden')
console.log(woodenHorse.getToyInfo())
const woodenBear = factory.produce('Bear',200,'wooden')
console.log(woodenHorse.getMaterialInfo())
//2


class Car {
    constructor(name,host) {
    this.name = name
    this.host = host
    }
    carSound(){
        return 'Usual car sound'
    }
}
 class AmbulanceCar extends Car {
     constructor(name) {
         super()
        this.name = name
     }
     ambulanceSound(){
    return 'Siren sound'
     }
 }


 const mercedes = new Car('Mercedes', 'Doctor')
const ambulanceMercedes = new AmbulanceCar(mercedes)
console.log(ambulanceMercedes.ambulanceSound())

const toyota = new Car('Toyota','Doctor2')
const ambulanceToyota = new AmbulanceCar('Toyota')
console.log(ambulanceToyota.ambulanceSound())