const loadByJSBtn = document.querySelector('.load_by_js')
const loadByFetch = document.querySelector('.load_by_fetch')
const leftColumn = document.querySelector('.left_column')
const rightColumn = document.querySelector('.right_column')
let url ='https://jsonplaceholder.typicode.com/users'
const spinner = document.getElementById('spinner');

function showSpinner() {
  spinner.className = 'show';
  setTimeout(() => {
    spinner.className = spinner.className.replace('show', '');
  }, 2000);
}

function hideSpinner() {
  spinner.className = spinner.className.replace('show', '');
}

loadByJSBtn.addEventListener('click', () => {
    
    showSpinner()

        let xhr = new XMLHttpRequest()
        xhr.open('GET',url)
        xhr.onreadystatechange = function(){
            if(xhr.status === 200 && xhr.readyState === 4){
                hideSpinner()
                    let responsed = JSON.parse(xhr.response)
                    console.log(responsed)
                   responsed.forEach(element => {
                        leftColumn.insertAdjacentHTML('beforeend', `
                        <div class="left_column_card">${element.name}</div>
                        `)
                })
            }
        }
            xhr.send()
})



const fetchDataMethods = async function (data) {
    let response
    switch(data.method){
        case 'GET':
        response = await fetch(data.url)
        break
    case 'DELETE':
        response = await fetch(data.url, {
            method: data.method
        })
        break
    default:
        response = await fetch(data.url, {
            method: data.method,
            body: JSON.stringify(data.body),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        });
        break;
    }
    if(response.ok){
        return await response.json()
    } else {
        throw new Error(`Error on adress ${url}, error status${response.status} `)
    }
} 

loadByFetch.addEventListener('click',() => {
showSpinner()
fetchDataMethods({
    method: 'GET',
    url:'https://jsonplaceholder.typicode.com/users'
})
.then((data) => {
    hideSpinner()
    data.forEach((element) => {
        let elementId = element.id;
        rightColumn.insertAdjacentHTML('beforeend',`
            <div class="record" data-record-id="${elementId}">
                <div class="record__username">${element.name}</div>
                
                <button class="record__delete"><span>Delete</span> </button>
                <div class="form"'>
                    <input type="text" value="${element.name}">
                    <button class="record__save"><span>Save</span></button>
                </div>
            </div>`
        ) 
              
        document.querySelector(`[data-record-id="${elementId}"] .record__save`).addEventListener('click', function (e) {
          
            showSpinner()
           
            let target = e.target;
            let recordId = target.closest('.record').dataset.recordId;
            fetchDataMethods({
                method: 'PUT',
                body: {
                    id: elementId,
                    name: target.closest('.form').querySelector('input').value
                },
                url: `https://jsonplaceholder.typicode.com/users/${recordId}`
            }).then((data) => {
                document.querySelector(`[data-record-id="${elementId}"] .record__save`) 
                showSpinner()
                document.querySelector(`[data-record-id="${elementId}"] .record__username`)
                .textContent = data.name.trim();
            });
        });
        document.querySelector(`[data-record-id="${elementId}"] .record__delete`)
        .addEventListener('click', function (e) {
            showSpinner()
            let target = e.target;
            let recordId = target.closest('.record').dataset.recordId;
            fetchDataMethods({
                method: 'DELETE',
                url: `https://jsonplaceholder.typicode.com/users/${recordId}`
            }).then(() => {
                alert(`${document.querySelector(`[data-record-id="${elementId}"] .record__username`)
                .textContent.trim()} deleted successfully!`);
                document.querySelector(`[data-record-id="${elementId}"] .record__delete`)
                showSpinner()
                document.querySelector(`[data-record-id="${elementId}"]`).remove();
            }).catch((e) => {
                console.log(e);
                document.querySelector(`[data-record-id="${elementId}"] .record__delete`)
                showSpinner()
            });
        });
    })
	}).catch((e) => {
		console.log(e); 
hideSpinner()
    })
 
})

   


