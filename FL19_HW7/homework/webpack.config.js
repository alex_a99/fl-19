const path = require('path')
const miniCss = require('mini-css-extract-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin')
module.exports = {
    mode: 'production',
    devServer: {
        historyApiFallback: true,
        contentBase: path.resolve(__dirname, './dist'),
        open: true,
        compress: true,
        hot: true,
        port: 8080,
    },
    entry: {
        main: ['@babel/polyfill','./src/index.js'],
    },
    output : {
        filename: 'bundle.js',
        path: path.resolve(__dirname,'dist')
    },
    plugins:
        [
            new HTMLWebpackPlugin({
                template: './src/index.html'
            }),
            new miniCss({
                filename: 'style.css',
             }),
        ],
    module:{
        rules: [{
           test:/\.scss$/,
           use: [
              miniCss.loader,
              'css-loader',
              'sass-loader',
           
            
           ]},
        {
            test: /\.m?js$/,
            exclude: /node_modules/,
            use: {
              loader: "babel-loader",
              options: {
                presets: ['@babel/preset-env']
            }
        }
        }],

    }
        
    
}